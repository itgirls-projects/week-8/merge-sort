import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] array = {"elephant", "cat", "leopard", "trout"};
        System.out.println("Initial array: " + Arrays.toString(array));
        System.out.println("-------");
        mergeSort(array);
        System.out.println("Sorted array: " + Arrays.toString(array));
    }

    public static void mergeSort(String[] array) {
        if (array.length < 2) {
            return;
        }

        int middleIndex = array.length / 2;
        String[] leftArray = new String[middleIndex];
        String[] rightArray = new String[array.length - middleIndex];

        for (int i = 0; i < middleIndex; i++) {
            leftArray[i] = array[i];
        }
        for (int i = middleIndex; i < array.length; i++) {
            rightArray[i - middleIndex] = array[i];
        }

        mergeSort(leftArray);
        mergeSort(rightArray);

        merge(array, leftArray, rightArray);
    }

    public static void merge(String[] outputArray, String[] leftArray, String[] rightArray) {
        int i = 0;
        int j = 0;
        int k = 0;

        while (i < leftArray.length && j < rightArray.length) {
            if (leftArray[i].length() <= rightArray[j].length()) {
                outputArray[k] = leftArray[i];
                i++;
            }
            else {
                outputArray[k] = rightArray[j];
                j++;
            }
            k++;
        }

        while(i < leftArray.length){
            outputArray[k] = leftArray[i];
            i++;
            k++;
        }

        while(j < rightArray.length){
            outputArray[k] = rightArray[j];
            j++;
            k++;
        }
    }
}